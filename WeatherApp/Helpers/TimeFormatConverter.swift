//
//  TimeFormatConverter.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/14/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import Foundation

class TimeFormatConverter {

    private static let timeFormatString = "hh:mm a"

    private static let gmtDateFormatter: DateFormatter = {
        $0.timeZone = TimeZone(abbreviation: "UTC")!
        $0.dateFormat = timeFormatString
        return $0
    }(DateFormatter())

    private static let localDateFormatter: DateFormatter = {
        $0.timeZone = TimeZone.current
        $0.dateFormat = timeFormatString
        return $0
    }(DateFormatter())


    /// Converts a UTC time string to a time string in the local time zone
    ///
    /// - Parameters:
    ///   - timeString: The UTC time string
    ///   - localTimeZone: Needed only for unit testing since setting NSTimeZone.default no longer seems to have an affect on the system's time zone
    /// - Returns: The time formatting for the local time zone
    static func convertGMTTimeStringToLocalTimeString(timeString: String, localTimeZone: TimeZone? = nil) -> String {
        guard let date = gmtDateFormatter.date(from: timeString) else { return timeString }

        if let timeZone = localTimeZone {
            localDateFormatter.timeZone = timeZone
        }

        return localDateFormatter.string(from: date)
    }
}
