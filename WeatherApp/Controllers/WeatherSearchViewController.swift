//
//  WeatherSearchViewController.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/9/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import UIKit
import SVProgressHUD

private let maxRecentCitiesCount = 10

class WeatherSearchViewController: UITableViewController {

    fileprivate var recentlySearchedCities: [String] = [] {
        didSet {
            // Persist to disk whenever the recentlySearchedCities change
            UserDefaults.persistedRecentlySearchedCities = recentlySearchedCities
        }
    }

    // Filtered cities are shown during an active search
    fileprivate var filteredCities = [String]()

    // If filteredCities is empty, either due to no matches or a search not taking place, then show all recentlySearchedCities
    private var citiesToDisplay: [String] {
        return filteredCities.isEmpty ? recentlySearchedCities : filteredCities
    }

    fileprivate let searchController = UISearchController(searchResultsController: nil)


    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Load from NSUserDefaults
        recentlySearchedCities = UserDefaults.persistedRecentlySearchedCities ?? []

        // Configure the searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self

        // Configure the searchBar
        searchController.searchBar.delegate = self
        searchController.searchBar.autocapitalizationType = .words
        searchController.searchBar.placeholder = "Enter a City Name"

        // Set the searchBar as the tableHeaderView
        tableView.tableHeaderView = searchController.searchBar

        SVProgressHUD.setDefaultStyle(.dark)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        tableView.reloadData()
    }


    // MARK: - UITableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return min(citiesToDisplay.count, maxRecentCitiesCount)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityNameCell", for: indexPath)
        let cityName = citiesToDisplay[indexPath.row]
        cell.textLabel?.text = cityName
        return cell
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return citiesToDisplay.isEmpty ? nil : "Last \(maxRecentCitiesCount) Searches"
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return (searchController.searchBar.text ?? "").isEmpty
    }


    // MARK: - UITableViewDelegate

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.recentlySearchedCities.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }


    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        searchController.isActive = false
        if identifier == "WeatherDetailViewControllerSegue" {
            guard let cell = sender as? UITableViewCell else { fatalError("Unknown Sender") }
            let cityName = cell.textLabel?.text ?? ""
            fetchWeatherForCityAndShowDetailView(cityName: cityName)
        } else {
            fatalError("Unknown Segue Identifier")
        }
        return false // The segue is handled manually in fetchWeatherForCityAndShowDetailView
    }


    /// Fetches the data for a city and navigates to the detail view
    ///
    /// - Parameter cityName: The city to fetch the weather for
    fileprivate func fetchWeatherForCityAndShowDetailView(cityName: String) {
        SVProgressHUD.show(withStatus: "Fetching weather for \(cityName)...")

        API.fetchWeatherData(for: cityName, onSuccess: { weatherData in
            SVProgressHUD.dismiss()

            // Update the recently searched cities
            self.addCityToRecentlySearchedCities(cityName: weatherData.cityName)

            // Navigate to the detail view
            guard let weatherDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "WeatherDetailViewController") as? WeatherDetailViewController else { fatalError() }
            weatherDetailViewController.configure(weatherData: weatherData)
            self.navigationController?.pushViewController(weatherDetailViewController, animated: true)
        }, onFailure: { errorMessage in
            SVProgressHUD.dismiss()
            self.presentAlertController(title: "Error", message: errorMessage)
        })
    }

    private func addCityToRecentlySearchedCities(cityName: String) {
        if let indexOfCity = recentlySearchedCities.index(of: cityName) {
            recentlySearchedCities.remove(at: indexOfCity)
        }

        recentlySearchedCities.insert(cityName, at: 0)

        if recentlySearchedCities.count > maxRecentCitiesCount {
            // Since maxRecentCitiesCount is adjustable, this will remove the proper number of cities
            let numCitiesToRemove = recentlySearchedCities.count - maxRecentCitiesCount
            recentlySearchedCities.removeLast(numCitiesToRemove)
        }
    }
}


// MARK: - UISearchBarDelegate

extension WeatherSearchViewController: UISearchBarDelegate {
    /// Handles tapping search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let potentialCityName = searchBar.text?.capitalized ?? ""

        searchController.isActive = false
        fetchWeatherForCityAndShowDetailView(cityName: potentialCityName)
    }
}

extension WeatherSearchViewController: UISearchResultsUpdating {
    /// Handles live filtering
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text ?? ""
        filteredCities = recentlySearchedCities.filter { $0.contains(searchText) }
        tableView.reloadData()
    }
}
