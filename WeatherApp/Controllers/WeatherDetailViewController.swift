//
//  WeatherDetailViewController.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/9/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class WeatherDetailViewController: UITableViewController {

    private var weatherData: WeatherData!

    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var cityNameLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var humidityLabel: UILabel!
    @IBOutlet private weak var weatherDescriptionLabel: UILabel!


    /// Initializes the required properties for the View Controller
    ///
    /// - Parameter weatherData: The weatherData object needed to for configuration
    func configure(weatherData: WeatherData) {
        self.weatherData = weatherData
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        assert(weatherData != nil, "Assertion Failure: weatherData must not be nil. Please use the configure(weatherData:) method to set the weatherData")

        refreshControl = UIRefreshControl()

        refreshControl?.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)

        cityNameLabel.text = weatherData.cityName
        timeLabel.text = weatherData.observationTime ?? "Unavailable"
        temperatureLabel.text = weatherData.temperature.map { "\($0) °C" } ?? "Unavailable"
        humidityLabel.text = weatherData.humidity ?? "Unavailable"
        weatherDescriptionLabel.text = weatherData.description ?? "Description Unavailable"

        if let iconUrl = weatherData.weatherIconUrl {
            Alamofire.request(iconUrl).responseImage { response in
                if let image = response.result.value {
                    self.iconImageView.image = image
                }
            }
        }
    }

    dynamic func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.beginRefreshing()

        API.fetchWeatherData(for: weatherData.cityName, onSuccess: { weatherData in
            self.refreshControl?.endRefreshing()
            self.weatherData = weatherData
            self.tableView.reloadData()
        }, onFailure: { errorMessage in
            self.refreshControl?.endRefreshing()
            self.presentAlertController(title: "Error", message: errorMessage)
        })
    }
}
