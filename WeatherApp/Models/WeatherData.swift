//
//  WeatherData.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/9/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import Foundation

struct WeatherData {
    var weatherIconUrl: String?
    var observationTime: String?
    var cityName: String
    var temperature: String?
    var humidity: String?
    var description: String?
}
