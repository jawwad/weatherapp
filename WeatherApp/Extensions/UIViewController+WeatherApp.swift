//
//  UIViewController+WeatherApp.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/14/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import UIKit

extension UIViewController {

    /// Helper method that configures and presents a UIAlertController
    ///
    /// - Parameters:
    ///   - title: The alert's title
    ///   - message: The alert's message
    func presentAlertController(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
