//
//  UserDefaults.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/13/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import Foundation

extension UserDefaults {
    private static let persistedRecentlySearchedCitiesKey = "persistedRecentlySearchedCitiesKey"

    private static var keySuffix: String = {
        if ProcessInfo.processInfo.environment["XCInjectBundleInto"] != nil {
            return "-runningUnitTests"
        } else if ProcessInfo.processInfo.arguments.contains("RunningUITests") {
            return "-runningUITests"
        } else {
            return ""
        }
    }()

    /// The array of the user's most recently searched cities saved to UserDefaults
    class var persistedRecentlySearchedCities: [String]? {
        get {
            return UserDefaults.standard.stringArray(forKey: persistedRecentlySearchedCitiesKey + keySuffix)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: persistedRecentlySearchedCitiesKey + keySuffix)
            UserDefaults.standard.synchronize()
        }
    }
}

public class TestHelper {
    class func clearPersistedCitiesForTests() {
        UserDefaults.persistedRecentlySearchedCities = []
    }
}
