//
//  AppDelegate.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/9/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
