//
//  API.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/9/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import Foundation
import Alamofire

class API {

    static private var weatherEndpointURLComponents = URLComponents(string: "https://api.worldweatheronline.com/free/v1/weather.ashx")!
    static private let commonQueryItems = [
        URLQueryItem(name: "format", value: "json"),
        URLQueryItem(name: "key", value: "vzkjnx2j5f88vyn5dhvvqkzc")
    ]

    /// Constructs the proper fetch URL given a city name
    ///
    /// - Parameter cityName: The city name to fetch the weather for
    /// - Returns: URL to fetch the city
    private class func constructFetchURL(forCity cityName: String) -> URL {
        let cityNameQueryItem = URLQueryItem(name: "q", value: cityName)
        weatherEndpointURLComponents.queryItems = commonQueryItems + [cityNameQueryItem]

        guard let requestURL = weatherEndpointURLComponents.url else { fatalError("Error constructing URL from components") }
        return requestURL
    }

    /// Fetches the weather data for the given city
    ///
    /// - Parameters:
    ///   - cityName: The city to fetch the weather for
    ///   - onSuccess: Upon success, called with the fetched WeatherData object
    ///   - onFailure: Called upon failure
    class func fetchWeatherData(for cityName: String, onSuccess: @escaping (WeatherData) -> Void, onFailure: @escaping (String) -> Void) {

        let requestURL = constructFetchURL(forCity: cityName)

        Alamofire.request(requestURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { response in
            switch response.result {
            case .success:

                // Ensure that the response has a "data" object within the top level response object
                guard
                    let responseJson = response.result.value as? [String: Any],
                    let dataJson = responseJson["data"] as? [String: Any]
                    else {
                        onFailure("Data was not received in the expected format and cannot be displayed")
                        return
                }

                // See if the response contains a valid error message
                if let errorJsonArray = dataJson["error"] as? [Any],
                    let errorJson = errorJsonArray.first as? [String: Any],
                    let errorMessage = errorJson["msg"] as? String {
                    onFailure(errorMessage)
                    return
                }


                // Response contains "data" but not "current_condition"
                guard
                    let currentConditionJsonArray = dataJson["current_condition"] as? [Any],
                    let currentConditionJson = currentConditionJsonArray.first as? [String: Any]
                    else {
                        onFailure("Unable to find current weather conditions in the response")
                        return
                }

                // Parse humidity, observationTime and temperature
                let humidity = currentConditionJson["humidity"] as? String
                let gmtObservationTime = currentConditionJson["observation_time"] as? String
                let observationTime = gmtObservationTime.map { TimeFormatConverter.convertGMTTimeStringToLocalTimeString(timeString: $0) } ?? gmtObservationTime
                let temperature = currentConditionJson["temp_C"] as? String

                // Parse weatherDescription
                var weatherDescription: String?
                if let weatherDescriptionArray = currentConditionJson["weatherDesc"] as? [Any],
                    let weatherDescriptionDict = weatherDescriptionArray.first as? [String: String] {
                    weatherDescription = weatherDescriptionDict["value"]
                }

                // Parse weatherIconUrl
                var weatherIconUrl: String?
                if let weatherIconUrlArray = currentConditionJson["weatherIconUrl"] as? [Any],
                    let weatherIconUrlDict = weatherIconUrlArray.first as? [String: String] {
                    weatherIconUrl = weatherIconUrlDict["value"]
                }

                // Parse the city name from the response, since searching for "France" returns "Paris, France"
                var responseCityName = cityName
                if let requestJsonArray = dataJson["request"] as? [Any],
                    let requestJsonObject = requestJsonArray.first as? [String: String] {
                    responseCityName = requestJsonObject["query"] ?? cityName
                }

                // Return the weatherData in the onSuccess callback
                let weatherData = WeatherData(weatherIconUrl: weatherIconUrl, observationTime: observationTime, cityName: responseCityName, temperature: temperature, humidity: humidity, description: weatherDescription)
                onSuccess(weatherData)
            case .failure(let error):
                onFailure(error.localizedDescription)
            }
        }
    }
}
