//
//  WeatherAppUITests.swift
//  WeatherAppUITests
//
//  Created by Jawwad Ahmad on 5/14/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherAppUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()

        continueAfterFailure = false

        let app = XCUIApplication()
        app.launchArguments = ["RunningUITests"]
        app.launch()
    }

    func testWeatherAppUI() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let cells = tablesQuery.cells

        // Tap on the search bar
        let enterACityNameSearchField = tablesQuery.searchFields["Enter a City Name"]
        enterACityNameSearchField.tap()

        // Enter Londor
        let enterACityNameSearchField2 = app.searchFields["Enter a City Name"]
        enterACityNameSearchField2.typeText("London")
        app.typeText("\r")

        // Go back
        let backButton = app.navigationBars["Current Weather"].buttons["Search"]
        backButton.tap()

        // Tap on the search bar again and enter newYork
        enterACityNameSearchField.tap()
        enterACityNameSearchField2.typeText("New York")
        app.typeText("\r")

        // Go back again
        backButton.tap()

        // Assert that the first cell shows New York since it was the most recently searched
        var firstCellLabel = cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        XCTAssertEqual(firstCellLabel, "New York")


        // Tap London and go back
        tablesQuery.staticTexts["London"].tap()
        backButton.tap()

        // Now assert that the first cell is London since it was the most recently searched
        firstCellLabel = cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        XCTAssertEqual(firstCellLabel, "London")


        // Verify that there are two cells
        XCTAssertEqual(cells.count, 2, "found instead: \(cells.debugDescription)")

        // Delete the 2nd cell
        cells.element(boundBy: 1).swipeLeft()
        cells.element(boundBy: 1).buttons["Delete"].tap()
        XCTAssertEqual(cells.count, 1, "found instead: \(cells.debugDescription)")

        // Delete the 1st cell
        cells.element(boundBy: 0).swipeLeft()
        cells.element(boundBy: 0).buttons["Delete"].tap()
        XCTAssertEqual(cells.count, 0, "found instead: \(cells.debugDescription)")
    }
}
