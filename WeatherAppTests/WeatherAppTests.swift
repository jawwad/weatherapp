//
//  WeatherAppTests.swift
//  WeatherAppTests
//
//  Created by Jawwad Ahmad on 5/9/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import WeatherApp


class WeatherAppTests: XCTestCase {

    private func stubUrl(_ requestUrl: String, withFile httpStubsFile: String) {
        OHHTTPStubs.stubRequests(passingTest: { request in
            return request.url?.host == "api.worldweatheronline.com"
        }, withStubResponse: { request in
            let fixture = OHPathForFile("HTTPStubs/\(httpStubsFile)", AppDelegate.self)!
            return OHHTTPStubsResponse(fileAtPath: fixture, statusCode: 200, headers: ["Content-Type": "application/json"]).requestTime(0, responseTime: 2)
        })
    }

    // Tests the API.fetchWeatherData method
    func test_fetchWeatherData() {
        let requestUrl = "http://api.worldweatheronline.com/free/v1/weather.ashx?key=vzkjnx2j5f88vyn5dhvvqkzc&format=json&q=London"

        // Use the "london.json" file to return the response
        stubUrl(requestUrl, withFile: "london.json")

        // Set up the expectation
        let weatherDataExpectation = expectation(description: "fetchWeatherData")
        var weatherData: WeatherData!
        API.fetchWeatherData(for: "London", onSuccess: { fetchedWeatherData in
            weatherData = fetchedWeatherData
            weatherDataExpectation.fulfill()
        }, onFailure: { _ in
            XCTFail("Fetch request failed: onFailure() called")
        })


        // Check assertions based on data in london.json file
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("Expectation timed out. Error: \(error.localizedDescription)")
            }

            XCTAssertEqual(weatherData.weatherIconUrl, "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png")
            let localTime = TimeFormatConverter.convertGMTTimeStringToLocalTimeString(timeString: "01:13 AM")
            XCTAssertEqual(weatherData.observationTime, localTime)
            XCTAssertEqual(weatherData.cityName, "London")
            XCTAssertEqual(weatherData.humidity, "82")
            XCTAssertEqual(weatherData.temperature, "13")
            XCTAssertEqual(weatherData.description, "Partly cloudy")
        }
    }
}
