//
//  TimeFormatConverterTests.swift
//  WeatherApp
//
//  Created by Jawwad Ahmad on 5/14/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

import XCTest
@testable import WeatherApp

class TimeFormatConverterTests: XCTestCase {

    private let gmtTimeString = "07:27 AM"

    func testTimeFormatConversionWhenInDubai() {
        let dubaiTimeZone = TimeZone(identifier: "Asia/Dubai")!
        let localTimeString = TimeFormatConverter.convertGMTTimeStringToLocalTimeString(timeString: gmtTimeString, localTimeZone: dubaiTimeZone)
        XCTAssertEqual(localTimeString, "11:27 AM")
    }

    func testTimeFormatConversionWhenInNewYork() {
        let newYorkTimeZone = TimeZone(identifier: "America/New_York")!
        let localTimeString = TimeFormatConverter.convertGMTTimeStringToLocalTimeString(timeString: gmtTimeString, localTimeZone: newYorkTimeZone)
        XCTAssertEqual(localTimeString, "02:27 AM")
    }
}
